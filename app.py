from http import HTTPStatus
from flask import Flask, abort, request, send_from_directory, make_response, render_template, redirect, flash, escape
from werkzeug.datastructures import WWWAuthenticate
import flask
from json import dumps, loads
from base64 import b64decode
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from markupsafe import escape
from werkzeug.security import generate_password_hash, check_password_hash 
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect


tls = local()
inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie. This is also used for the CSRF protection to sign the token. 
app.secret_key = '192b9bdd22ab9ed4d12e236c78afcb9a393ec15f71bbf5dc987d54727823bcbf'

# configuring SQLAlchemy 
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///mydatabase.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# initializing the app with the database
db = SQLAlchemy(app)

# enabling CSRFProtect to the app
csrf = CSRFProtect()
csrf.init_app(app)

# Add a login manager to the app
import flask_login
from flask_login import login_required, login_user, logout_user, current_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
# Also used to define the model for the database
class User(db.Model, flask_login.UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(80))

# creating the user table after defining the model above.
db.create_all()


# This method is called whenever the login manager needs to get
# the User object for a given user id  user_id not in users:
@login_manager.user_loader
def user_loader(user_id):
    exists = User.query.filter_by(id=user_id).first()
    if exists is False:
        return

    # load the User from the database
    return User.query.get(int(user_id))


# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')
        u = User.query.filter_by(id=uid).first()
        if u and check_password_hash(u.password, passwd):#and users[uid]['password'] == passwd:  #users.get(pwd) #check_password_hash(p, users.get(u)):
            return user_loader(uid)
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        print(f'Bearer auth: {auth_params}')
        
        #User.query.filter_by(token = auth_params).first()
        #    return user_loader(uid)
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'


@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return render_template('index.html')


@app.route('/signup')
def signup_route():
    return render_template('signup.html')


@app.route('/signup', methods = ['GET', 'POST'])
def signup():
        username = format(escape(request.form.get('username').lower()))
        password = format(escape(request.form.get('password')))
        confirm_password = format(escape(request.form.get('Confirm Password')))
        u = User.query.filter_by(username=username).first()

        username_length = len(username) == 0
        password_length = len(password) == 0
        conpass_length = len(confirm_password) == 0

        if u:
            flash('username already in use')
            return redirect(flask.url_for('signup'))
        
        elif (len(password) < 8):
            flash('password length needs to be at least 8 characters long')
            return redirect(flask.url_for('signup'))
        
        elif (password != confirm_password):
            flash('password and confirm password not the same')
            return redirect(flask.url_for('signup'))

        elif (username_length or password_length or conpass_length):
            flash("please provide password and username")
            return redirect(flask.url_for('signup'))
        
        else:
            user = User(username=username, password=generate_password_hash(password, method='sha256', salt_length=8))
            db.session.add(user)
            db.session.commit()
            
            flash('signed up successfully')

            return redirect(flask.url_for('login'))


@app.route('/login')
def login_route():
    return render_template('login.html')


@app.route('/login', methods=['GET','POST'])
def login():
        username = format(escape(request.form.get('username').lower()))
        password = format(escape(request.form.get('password')))
        u = User.query.filter_by(username=username).first() 

        if u and check_password_hash(u.password, password): 
            user = user_loader(u.id)
            
            # automatically sets logged in session cookie
            login_user(user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not flask.is_safe_url(next) and not current_user.is_authenticated:
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index_html'))
        else:
            flash('wrong username or password')
            return flask.redirect(flask.url_for('login'))


@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash("you are logged out")
    return redirect(flask.url_for('login'))


@app.get('/messages')
@login_required
def messages():
    sender = current_user.username
    stmtsearchprep = f"SELECT * FROM messages WHERE sender=? or recipient=?"

    result = f"Query: {pygmentize(stmtsearchprep)}\n"
    try:
        c = conn.execute(stmtsearchprep, (sender, sender))
        rows = c.fetchall()
        result = result + 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET'])
@csrf.exempt
@login_required
def send():
    try:
        recipient = format(escape(request.args.get('recipient') or request.form.get('recipient')))
        
        recipient_user = User.query.filter_by(username=recipient).first()
    
        sender = current_user.username
        message = format(escape(request.args.get('message') or request.args.get('message')))
        if not sender or not message or not recipient_user: 
            return f'ERROR: missing sender or message or recipient is not a valid user'
        stmt_prep = f"INSERT INTO messages (sender, recipient, message) values (?, ?, ?);"
        result = f"Query: {pygmentize(stmt_prep)}\n"
        conn.execute(stmt_prep, (sender, recipient, message))
        return f'{result}ok'
    except Error as e:
        return f'{result}ERROR: {e}'


@app.get('/announcements')
@login_required
def announcements():
    try:
        author = current_user.username

        stmt = f"SELECT author,text FROM announcements where author=?;"
        c = conn.execute(stmt, (author, ))
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}


@app.get('/coffee/')
def nocoffee():
    abort(418)

@app.route('/coffee/', methods=['POST','PUT'])
def gotcoffee():
    return "Thanks!"

@app.get('/highlight.css')
def highlightstyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp

try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        recipient TEXT NOT NULL,
        message TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
except Error as e:
    print(e)
    sys.exit(1)



        

