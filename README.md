
# Design Considerations and Code Structure

The code structure is really bad where everything is in one file. For example, the authorization and login is in the same file as the initialization of the flask app, the database creation and the url routes are also in this file. The code structue would be much better if these were placed in seperate files, so that the code would be much more clean and structured and also making it easier to catch bugs and security issues. 
Another really big issue is the storing of the data, specially the users information. The users information is only stored as a nested dictionary where everything is stored as plaintext including username and the corresponding password. This is a major security issue as the application is very vulnerable to leaking user information, which is something we as developers of the application should guarantee to not happen to the users. Another major security issue is that the login method does not actually have any password checking, so anyone can log into any of the users. For example the user Bob can also log into the user Alice without providing the right password. This is very bad authentication. And when you are logged in as any of the users, you can also send a message as any other user, so for example if the user Bob is logged in, he can send a message where Alice will be seen as the sender. This is very bad authorization, where the application gives any user the permission to send a message proposing to be someone else and the permission to see the other users messages. The application is also vulnerable to SQL injection in the send, search and announcements function. The functions uses SQL queries to access and insert data from the tiny database, and when doing this the queries use values that come directly from user input, without validating or sanitizing. This enables an attacker to send their own malicious SQL queries to the database, making the integrity, confidentiality and authentication of the application being compromised.


# Features of the application
With the application you can send messages to other users, recieve messages that have been sent to you and see all messages you have sent and recieved. 

# How to test/demo it
Clone the project or get it from git any other way.
Get to the project folder in the terminal by getting to the right directory.
In addition to everything that you need from the login-server exercise, you also need to install Flask-SQLAlchemy. Which can be done by writing the command, provided under here, in the terminal. 
Then run the web-server with the command `python -m flask run` and open your web browser and go to http://localhost:5000/signup. If you have already registered a user you can go directly to http://localhost:5000/login. 
Register a new user and you will be redirected to login page where you can login with the user you just created. Then you will get to the messaging page or home page, where you can either send a message, by providing a recipient, writing some text and then clicking on send, you can also see all messages that you have sent and recieved by clicking on messages or Show all. If you want to log out you can do so by clicking on Logout button.


```shell
pip install flask-sqlalchemy==2.5.1
```

# Technical Details 

I have chosen to use the Flask-SQLAlchemy for creating, storing and accessing the users information. I have defined the users model in the class User where it will create a table users with the relevant information such as username, password and they will get an id number which will be the primary key for the table. Then I use the db.create_all() function from SQLAlchemy to create the tables that I have defined. I also use SQLAlchemy for querying over data in the users table and for creating new users and adding them in the table in the signup function. I also have chosen werkzeug.security for hashing passwords and checking passwords. In addition I have used the CSRFProtect extension to enable CSRF protection globally in the app. 


# Answers to the questions

The application can be attacked by anyone in many different ways. One type of attack can be to perform a distributed denial-of-service. With this attack the attacker is exploiting the server by using many bots that will be simultaneously send a large amount of request to the server causing the server to crash and not respond to the requests even the requests that are coming from real users that are trying to use the application. This type of an attack is damaging the availability of the web application. Availability is an important part of the security requirements to an application, since without this the users do not have access to use the application at all. This attack is more of an attack against the server side of the application and not so much on the web application itself, but it's still a flaw from the web application and it is in the web application the prevention and mitigation from this type of attack should be in. The DDOS attack can be done by anyone and the attacker doesn't really need to be an expert on hacking and attacking websites. 


Another attack that the application can be vulnerable to is Cross-site scripting attacks, which is an type of attack that is directly against the website and its application. This is one of the most common attacks against websites, so it is very crucial to have the right prevention and mitigation strategies on your website to defend against it. This attack can be performed by injecting malicious code into trusted websites and then getting the web-server to serve the malicious code on other users browsers. This allows the attacker to access resources from vitim server. Websites are usually vulnerable to cross-site scripting attacks when the websites use user input to generate some output without validating the user input. This type of an attack affects the data confidentiality and integrity of the web application, because the attacker can exploit the vulnerability and do many things such as steal session cookies and trick users to give their username and password by giving them a fake login form.
A similar attack that the application can be vulnerable to is Cross-site request forgery. This is an attack where the attacker tricks an authenticated user into performing a malicious action against themselves. Web forms and submit buttons are usually used to acheived this attack, and they are therefore very vulnerable. Web applications that uses session cookies to authenticate a user are common targets for CSRF attacks, because the web browsers will store the cookies and that will make it possible for the attacker to access and use this cookie to send a malicious request. 


Another attack that the application is vulnerable to is SQL injection. Websites are vulnerable to this type of attack when the web application uses user input directly into a SQL query to access information from a database. Similar to the cross-site scripting attacks, websites are vulnerable to this type of attack, when developers omit to validate and sanitize the user input before using it to procude some output. When an attacker sends their malicious input or code, the SQL query will execute, and the attacker can then cause severe damage to the application. The data integrity of the application can then be compromised by the attack, and also the availability since the attacker can delete the whole database, making users not able to access their resources. The confidentiality can also be compromised by a SQL injection, since an attacker can read and access sensitive data like passwords, bank details and much more. 


Even though attackers have all these possible ways to attack the website the attacker are also sometimes limited to what they can do. For example, with the DDOS attack, an attacker might need a network of bots to perform the requests against the server and getting such a network can be difficult. There might also be some protections and mitigations against such attacks on web applications and their servers, such as CAPTCHA where they will perform some test to verify whether the user is a robot or a real human. 
When it comes to the other attacks, the attacker usually needs to be very intelligent and creative, because the developers are usually the ones that should know and thought of everything when it comes to bugs and security flaws in their program and should therefore be able to fix and prevent any attack that might be possible. So, an attacker really needs to be both creative and intelligent, if the application is well developed, to be able find something that the developers haven't. 
For some of the attacks the web application needs to have some user input forms, so that the attacker can try to either perform a cross-site scripting attack, SQL injection or cross-site request forgery. The attacks can also be performed when the web application uses user input in HTTP requests. 


To protect the web application from these attacks I have used many different strategies. For the SQL injection attacks I have used prepared statements to execute the queries. With these prepared statements we can prevent SQL injections by seperating the user input and the SQL code. The SQL code first gets compiled without providing the user input values, and when it gets ready for execution, the values are supplied and we get the desired result. 
For the cross-site scripting attacks, we have Flask that configures Jinja2 to automatically escape all values unless we explicitly tell it otherwise. I have used templates to render HTML and with Flask we get every data automatically escaped that is rendered in the templates. This means that user input is safe from XSS and we can render and display it to the browser  In addition I have used the escape method from Flask to escape the username and password values that I get from the signup form and similarly in the login method. Escaping the values means to remove the possibility for the values to include any arbitrary HTML tags, so that an attacker can't inject any HTML with malicious Javascript code in it. To protect against CSRF attacks I have used the CSRF token from the CSRFProtect extension and added the tokens in the login and signup html forms. With this CSRF token in the forms the server will generate a token, which is random and unguessable string, and use this for further authentication. The CSRF protetion is also enabled globally so every GET and POST requests are also CSRF protected with the generated CSRF token, which prevents any attacker to manage a CSRF attack by exploiting the requests to do a malicious action. 
I also wanted to refactor and structure the code, so that everything was not in one file, but I did not have enough time for this. I would have put all of the initialization such app, database and CSRF initialization to a different file. I would also define the databases in another file, and put all of the login, logout and signup code into a file of its own. This would make the code more structured, easier to read and less vulnerable to security issues. 


The access control model used in the application is a type of mandatory access control where all users have been given the rights to see all messages in the database, and write any message where the recipient can be any other user. With this mandatory access control the system administrator or application developer has control over the rights a user has to an object or some data, and the users can't change these rights to either restrict them or extend them. And with this application the system administrator has chosen to give every users the rights to see all data in the messages database and insert any data into the database. 

The application now has some data traceability where every message has a sender (creator), so we now have some accountability towards a user for every message. Even though traceability breaks the end-to-end privacy between the recipient and sender, we sometimes still need traceability to ensure the validity of a message by checking its history from when it was created to where it is now.





# Resources I used for this project

https://flask.palletsprojects.com/en/2.2.x/api/#

https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/quickstart/

https://flask.palletsprojects.com/en/2.2.x/tutorial/database/

https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/queries/

https://en.wikipedia.org/wiki/Mandatory_access_control

https://flask-wtf.readthedocs.io/en/0.15.x/csrf/

https://flask.palletsprojects.com/en/2.2.x/tutorial/templates/

https://flask.palletsprojects.com/en/2.2.x/security/

https://en.wikipedia.org/wiki/Prepared_statement

https://cheatsheetseries.owasp.org/cheatsheets/Threat_Modeling_Cheat_Sheet.html

